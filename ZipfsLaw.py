import matplotlib.pyplot as plt
import math
import numpy as np
from scipy.optimize import curve_fit


def cut_high_freq(frequencies=None, ranks=None, valor=None):
    new_freq = []
    new_ranks = []

    for idx, freq in enumerate(frequencies):
        if freq < valor:
            new_freq.append(freq)
            new_ranks.append(ranks[idx])

    return new_freq, new_ranks


def cut_low_freq(frequencies=None, ranks=None, valor=None):
    new_freq = []
    new_ranks = []

    for idx, freq in enumerate(frequencies):
        if freq > valor:
            new_freq.append(freq)
            new_ranks.append(ranks[idx])

    return new_freq, new_ranks


def mainFitting(x, a, c):
    f = c / pow(x, a)
    return f


def logFitting(x, a, c):
    return c + (a * x)


# Read file
with open('filtered-output.txt', 'r') as file:
    data = file.readlines()

frequencies = []
ranges = []

logFrecs = []
logRanges = []

# Iterate through the data and get the frequency and range of each row
for i, row in enumerate(data):
    frequency = float(row.split(",")[0])

    range = len(data) - i
    ranges.append(range)

    frequencies.append(frequency)

    logRanges.append(math.log2(range))
    logFrecs.append(math.log2(frequency))

# If needed, cut some frequencies
k = 10000
new_frequencies, new_ranges = cut_high_freq(frequencies, ranges, k)
k = 8
new_frequencies, new_ranges = cut_low_freq(new_frequencies, new_ranges, k)

k2 = math.log2(10000)
newLogFreq, newLogRanges = cut_high_freq(logFrecs, logRanges, k2)
k2 = math.log2(8)
newLogFreq, newLogRanges = cut_low_freq(newLogFreq, newLogRanges, k2)

# Curve fitting
poptMain, pcovMain = curve_fit(mainFitting, ranges, frequencies)
poptLog, pcovLog = curve_fit(logFitting, logRanges, logFrecs)

# Curve fitting frequencies cut
poptMain2, pcovMain2 = curve_fit(mainFitting, new_ranges, new_frequencies)
poptLog2, pcovLog2 = curve_fit(logFitting, newLogRanges, newLogFreq)

# Create a figure with 2 subplots
fig, axes = plt.subplots(2, 2)

# Plot non-scaled data with logaritmic axis
axes[0, 0].plot(ranges, frequencies)
axes[0, 0].set_title("Main Plot")
axes[0, 0].plot(ranges, mainFitting(ranges, *poptMain), color='red')
axes[0, 0].set_xscale('log')
axes[0, 0].set_yscale('log')

axes[0, 1].set_title("Cut >10.000 freq, cut < 8 freq")
axes[0, 1].plot(new_ranges, new_frequencies)
axes[0, 1].plot(new_ranges, mainFitting(new_ranges, *poptMain2), color='red')
axes[0, 1].set_xscale('log')
axes[0, 1].set_yscale('log')

axes[1, 0].plot(logRanges, logFrecs)
axes[1, 0].set_title("Logarithmic data")
axes[1, 0].plot(logRanges, logFitting(np.array(logRanges), *poptLog), color='red')

axes[1, 1].set_title("Cut >10.000 freq, cut < 8 freq")
axes[1, 1].plot(newLogRanges, newLogFreq)
axes[1, 1].plot(newLogRanges, logFitting(np.array(newLogRanges), *poptLog2), color='red')
# Show data
plt.show()

print(*poptMain)
print(*poptMain2)
print(*poptLog)
print(*poptLog2)