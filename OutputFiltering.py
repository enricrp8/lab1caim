import re

with open('counted-words.txt', 'r') as input:
    lines = input.readlines()


final_document = ""

for idw, line in enumerate(lines):
    if idw < len(lines) - 2:
        palabra = line.split(" ")[-1]
        palabra = palabra[0:-1]

        if re.fullmatch("[a-zA-Z]+", palabra):
            print(palabra)
            final_document += line

print(final_document)

with open('filtered-output.txt', 'w') as output:
    output.write(final_document)
