import subprocess
from os import listdir, path
import os
import time
import matplotlib.pyplot as plt
import math
from scipy.optimize import curve_fit
import numpy as np


kFoldersNamesStruct = [
    ("novels", "novels"),
    ("arxiv_abs", "arxiv_abs/arxiv"),
    ("20_newsgroups", "20_newsgroups")
]


def mainFitting(x, a, b):
    return a * pow(x, b)


def logFitting(x, a, b):
    return a + x*b


def countWords(wordList):
    sum = 0
    for word in wordList:
        aux = word.split(",")
        sum += int(aux[0])
    return sum


def setUp():
    if(path.exists("./heap/novels")):
        subprocess.call(["rm", "-rf", "./heap"])
    subprocess.call(
        "mkdir ./heap", shell=True)


def setUpFolder(name):
    subprocess.call("mkdir ./heap/" + name, shell=True)
    subprocess.call("cp -r ./" + name + "/* ./heap/" + name, shell=True)


setUp()

for folderNameStruct in kFoldersNamesStruct:
    setUpFolder(folderNameStruct[0])

    files = listdir("./heap/" + folderNameStruct[1])

    diffWordsCounts = []
    textSizes = []

    logDiffWordsCounts = []
    logTextSizes = []

    i = 0
    while len(files) != 0:
        subprocess.check_output(
            "python3 IndexFiles.py --index " + folderNameStruct[0] + str(i) + " --path heap/" + folderNameStruct[0] + "/", shell=True)

        time.sleep(1)

        wordCount = subprocess.check_output(
            "python3 CountWords.py --index " + folderNameStruct[0] + str(i), shell=True)

        words = wordCount.decode("utf-8").split('\n')

        diffWordCount = int(words[-2].split(" ")[0])
        textSize = countWords(words[0:-3])

        diffWordsCounts.append(diffWordCount)
        textSizes.append(textSize)
        logDiffWordsCounts.append(math.log2(diffWordCount))
        logTextSizes.append(math.log2(textSize))

        file = files[-1]
        subprocess.call("rm  -r ./heap/" + folderNameStruct[1] +
                        "/" + str(file), shell=True)
        files.pop()

        i = i + 1

    poptMain, pcovMain = curve_fit(mainFitting, textSizes, diffWordsCounts)
    poptLog, pcovLog = curve_fit(logFitting, logTextSizes, logDiffWordsCounts)

    # Create a figure with 2 subplots
    fig, (mainPlot, logPlot) = plt.subplots(1, 2)
    fig.suptitle("Heaps' Law of " + folderNameStruct[0])

    # Plot non-scaled data with logaritmic axis
    mainPlot.plot(textSizes, diffWordsCounts)
    mainPlot.set_title("Main Plot")
    mainPlot.plot(textSizes, mainFitting(textSizes, *poptMain), color='red')

    # Plot data scaled logaritmically
    logPlot.plot(logTextSizes, logDiffWordsCounts)
    logPlot.plot(logTextSizes, logFitting(
        np.array(logTextSizes), *poptLog), color='red')
    logPlot.set_title("Log Plot")

    print(folderNameStruct[0] + ":")
    print(*poptMain)
    print(*poptLog)
    print('\n')


# diffWordsCounts = [61825, 60913, 58525, 59252, 59092, 58525, 58525, 58525, 56598, 56766, 56598, 52160, 51757, 51435, 50236,
#                    49238, 48060, 48048, 47702, 47061, 46412, 43291, 41497, 41070, 35343, 34236, 31940, 30074, 26192, 20178, 19708, 18377, 9992]
# textSizes = [3147864, 3092088, 2786989, 2875955, 2843059, 2786989, 2786989, 2786989, 2595108, 2623178, 2595108, 2205326, 2134522, 2088429, 1898644,
#              1815562, 1727665, 1723898, 1688091, 1633188, 1592519, 1465956, 1384461, 1361753, 1251005, 1191749, 1089355, 929462, 811436, 503790, 489375, 454068, 94870]


plt.show()
